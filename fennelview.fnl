;; A pretty-printer that outputs tables in Fennel syntax.
;; Loosely based on inspect.lua: http://github.com/kikito/inspect.lua

(local bs (string.char 92)) ; shouldn't be necessary but "\\" doesn't parse
(local quote (fn [str] (.. "\"" (: str :gsub "\"" (.. bs "\"")) "\"")))

(local short-control-char-escapes
       {"\a" "\\a" "\b" "\\b" "\f" "\\f" "\n" "\\n"
        "\r" "\\r" "\t" "\\t" "\v" "\\v"})

(local long-control-char-esapes {})

(for [i 0 31]
  (let [ch (string.char i)]
    (when (not (. short-control-char-escapes ch))
      (tset short-control-char-escapes ch (.. bs i))
      (tset long-control-char-esapes ch (: "\\%03d" :format i)))))

(local escape (fn [str]
                (let [str (: str :gsub bs (.. bs bs))
                      str (: str :gsub "(%c)%f[0-9]" long-control-char-esapes)]
                  (: str :gsub "%c" short-control-char-escapes))))

(local sequence-key? (fn [k len]
                       (and (= (type k) "number")
                            (<= 1 k)
                            (<= k len)
                            (= (math.floor k) k))))

(local type-order {:number 1 :boolean 2 :string 3 :table 4
                   :function 5 :userdata 6 :thread 7})

(local sort-keys (fn [a b]
                   (let [ta (type a) tb (type b)]
                     (if (and (= ta tb)
                              (or (= (type ta) "string")
                                  (= (type ta) "number")))
                         (< a b)
                         (let [dta (. type-order a)
                               dtb (. type-order b)]
                           (if (and dta dtb)
                               (< dta dtb)
                               dta true
                               dtb false
                               :else (< ta tb)))))))

(local get-sequence-length
       (fn [t]
         (let [len 1]
           (each [i (ipairs t)] (set len i))
           len)))

(local get-nonsequential-keys
       (fn [t]
         (let [keys {}
               sequence-length (get-sequence-length t)]
           (each [k (pairs t)]
             (when (not (sequence-key? k sequence-length))
               (table.insert keys k)))
           (table.sort keys sort-keys)
           (values keys sequence-length))))

(local count-table-appearances
       (fn recur [t appearances]
         (if (= (type t) "table")
             (when (not (. appearances t))
               (tset appearances t 1)
               (each [k v (pairs t)]
                 (recur k appearances)
                 (recur v appearances)))
             (when (= t t) ; no nans please
               (tset appearances t (+ (or (. appearances t) 0) 1))))
         appearances))



(local put-value nil) ; mutual recursion going on; defined below

(local puts (fn [out ...]
              (each [_ v (ipairs [...])]
                (table.insert out.buffer v))))

(local tabify (fn [out] (puts out "\n" (: out.indent :rep out.level))))

(local already-visited? (fn [out v] (~= (. out.ids v) nil)))

(local get-id (fn [out v]
                (let [id (. out.ids v)]
                  (when (not id)
                    (let [tv (type v)]
                      (set id (+ (or (. out.max-ids tv) 0) 1))
                      (tset out.max-ids tv id)
                      (tset out.ids v id)))
                  (tostring id))))

(local put-sequential-table (fn [out t length]
                              (puts out "[")
                              (set out.level (+ out.level 1))
                              (for [i 1 length]
                                (puts out " ")
                                (put-value out (. t i)))
                              (set out.level (- out.level 1))
                              (puts out " ]")))

(local put-key (fn [out k]
                 (if (and (= (type k) "string") (not (: k :find "%W")))
                     (puts out ":" k)
                     (put-value out k))))

(local put-kv-table (fn [out t]
                      (puts out "{")
                      (set out.level (+ out.level 1))
                      (each [k v (pairs t)]
                        (tabify out)
                        (put-key out k)
                        (puts out " ")
                        (put-value out v))
                      (set out.level (- out.level 1))
                      (tabify out)
                      (puts out "}")))

(local put-table (fn [out t]
                   (if (already-visited? out t)
                       (puts out "#<table " (get-id out t) ">")
                       (>= out.level out.depth)
                       (puts out "{...}")
                       :else
                       (let [(non-seq-keys length) (get-nonsequential-keys t)]
                         (if (> (. out.appearances t) 1)
                             (puts out "#<" (get-id out t) ">")
                             (= (# non-seq-keys) 0)
                             (put-sequential-table out t length)
                             :else
                             (put-kv-table out t))))))

(set put-value (fn [out v]
                 (let [tv (type v)]
                   (if (= tv "string")
                       (puts out (quote (escape v)))
                       (or (= tv "number") (= tv "boolean") (= tv "nil"))
                       (puts out (tostring v))
                       (= tv "table")
                       (put-table out v)
                       :else
                       (puts out "#<" tv " " (get-id out v) ">")))))



(fn [root options]
  (let [options (or options {})
        out {:appearances (count-table-appearances root {})
             :depth (or options.depth 128)
             :level 0 :buffer {} :ids {} :max-ids {}
             :indent (or options.indent "  ")}]
    (put-value out root)
    (table.concat out.buffer)))
