(local fennel (dofile "fennel.lua"))
(local inspect (fennel.dofile "fennelview.fnl"))

(local generate nil)

(let [seed (or (os.getenv "SEED") (os.time))]
  (print :seed seed)
  (math.randomseed seed))

(local random-char
       (fn []
         (if (> (math.random) 0.9)
             (string.char (+ 48 (math.random 10)))
             (> (math.random) 0.5)
             (string.char (+ 97 (math.random 26)))
             (> (math.random) 0.5)
             (string.char (+ 65 (math.random 26)))
             (> (math.random) 0.5)
             (string.char (+ 32 (math.random 15)))
             :else
             (string.char (+ 58 (math.random 5))))))

(local generators {:number (fn [] ; weighted towards mid-range integers
                             (if (> (math.random) 0.9)
                                 (let [x (math.random math.huge)]
                                   (math.floor (- x (/ x 2))))
                                 (> (math.random) 0.2)
                                 (math.floor (math.random 2048))
                                 :else (math.random)))
                   :string (fn []
                             (let [s ""]
                               (for [_ 1 (math.random 16)]
                                 (set s (.. s (random-char))))
                               s))
                   :table (fn [table-chance]
                            (let [t {} k nil]
                              (for [_ 1 (math.random 16)]
                                ;; no nans plz
                                (set k (generate 0.9))
                                (*while (~= k k) (set k (generate 0.9)))
                                (tset t k (generate (* table-chance 1.5))))
                              t))
                   :boolean (fn [] (> (math.random) 0.5))})

(set generate
     (fn [table-chance]
       (set table-chance (or table-chance 0.5))
       (if (> (math.random) 0.5) (generators.number)
           (> (math.random) 0.5) (generators.string)
           (> (math.random) table-chance) (generators.table table-chance)
           :else (generators.boolean))))

(local table-equal?
       (fn [a b deep-equal?]
         (let [mismatch-a {} mismatch-b {} inequal? false
               len (fn [t] (let [c 0] (each [_ (pairs t)] (set c (+ c 1))) c))]
           (each [k v (pairs a)]
             (when (deep-equal? v (. b k))
               (tset a k nil)
               (tset b k nil)))
           (each [k v (pairs a)]
             ;; don't bother with table keys; float keys can be unstable
             (when (not (= (type k) "table"))
               (tset mismatch-a (inspect k) v)))
           (each [k v (pairs b)]
             (when (not (= (type k) "table"))
               (tset mismatch-b (inspect k) v)))
           (or (and (= (len a) 0) (= (len b) 0))
               (deep-equal? mismatch-a mismatch-b)))))

(local deep-equal?
       (fn deep-equal? [a b]
         (or (~= a a) (~= b b) ; nan
             (and (= (type a) (type b))
                  (if (= (type a) "table")
                      (table-equal? a b deep-equal?)
                      (= (tostring a) (tostring b)))))))

(for [_ 1 256]
  (let [item (generate)
        inspected (inspect item)
        (ok evaled) (pcall fennel.eval inspected)]
    (when (or (not ok) (not (deep-equal? item evaled)))
      (print "failed:" (inspect item) (pp evaled)))))
