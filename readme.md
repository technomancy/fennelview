# Fennelview

A Fennel pretty-printer.

**Update**: this is becoming
  [part of Fennel](https://github.com/bakpakin/Fennel/issues/39); this
  repo will no longer be updated.

Put this in `~/.fennelrc`:

```
(set options.pp ((. (require "fennel") "dofile") "/path/to/fennelview/fennelview.fnl"))
```



Copyright © 2018 Phil Hagelberg
Released under the MIT license
